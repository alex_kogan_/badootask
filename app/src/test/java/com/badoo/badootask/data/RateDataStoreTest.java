package com.badoo.badootask.data;

import com.badoo.badootask.BuildConfig;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.robolectric.RobolectricGradleTestRunner;
import org.robolectric.RuntimeEnvironment;
import org.robolectric.annotation.Config;

import static junit.framework.Assert.assertEquals;

/**
 * Created by alexkogan on 21/05/2016.
 */
@RunWith(RobolectricGradleTestRunner.class)
@Config(constants = BuildConfig.class, sdk = 18)
public class RateDataStoreTest  {

    private RateDataStore rateDataStore;

    @Before
    public void setUp() {
        rateDataStore = RateDataStore.with(RuntimeEnvironment.application);
    }

    @Test
    public void testCalcConvertedAmount() throws Exception {
        assertEquals(false, true);
    }
}