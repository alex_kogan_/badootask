package com.badoo.badootask.util;

import rx.Subscriber;

/**
 * Created by alexkogan on 24/01/2016.
 */
public class BaseSubscriber extends Subscriber {
    private String mTag;

    public BaseSubscriber(String tag) {
        mTag = tag;
    }

    @Override
    public void onCompleted() {
        Logger.debug(mTag, "Completed");
    }

    @Override
    public void onError(Throwable e) {
        if (e != null) {
            Logger.error(mTag, "Error '%s'", e.getLocalizedMessage());
        }
    }

    @Override
    public void onNext(Object o) {

    }

}
