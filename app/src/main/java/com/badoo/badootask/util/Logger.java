package com.badoo.badootask.util;

import android.util.Log;

import org.json.JSONObject;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class Logger {
    private static int sLogLevel = 4;
    private static Delegate sCallback;

    public Logger() {
    }

    public static void setCallback(Delegate callback) {
        sCallback = callback;
    }

    public static void json(String tag, JSONObject jsonObject) {
        if(sLogLevel <= 3) {
            if(sCallback != null) {
                sCallback.json(tag, jsonObject);
            } else {
                Log.e(tag, jsonObject.toString());
            }

        }
    }

    public static void debug(String tag, String text) {
        if(sLogLevel <= 3) {
            if(sCallback != null) {
                sCallback.debug(tag, text);
            } else {
                Log.d(tag, text);
            }

        }
    }

    public static void info(String tag, String text) {
        if(sLogLevel <= 4) {
            if(sCallback != null) {
                sCallback.info(tag, text);
            } else {
                Log.i(tag, text);
            }

        }
    }

    public static void error(String tag, String text) {
        if(sLogLevel <= 6) {
            if(sCallback != null) {
                sCallback.error(tag, text);
            } else {
                Log.e(tag, text);
            }

        }
    }

    public static void info(String tag, String format, Object... args) {
        if(sLogLevel <= 4) {
            if(sCallback != null) {
                sCallback.info(tag, format, args);
            } else {
                Log.i(tag, String.format(format, args));
            }

        }
    }

    public static void debug(String tag, String format, Object... args) {
        if(sLogLevel <= 3) {
            if(sCallback != null) {
                sCallback.debug(tag, format, args);
            } else {
                Log.d(tag, String.format(format, args));
            }

        }
    }

    public static void error(String tag, String format, Object... args) {
        if(sLogLevel <= 6) {
            if(sCallback != null) {
                sCallback.error(tag, format, args);
            } else {
                Log.e(tag, String.format(format, args));
            }

        }
    }

    public static void logException(String tag, Throwable throwable) {
        if(sLogLevel <= 6) {
            if(sCallback != null) {
                sCallback.logException(tag, throwable);
            } else {
                Log.e(tag, throwable.getLocalizedMessage(), throwable);
            }

        }
    }

    public static void setMinLogLevel(int logLevel) {
        sLogLevel = logLevel;
    }

    public interface Delegate {
        void json(String var1, JSONObject var2);

        void debug(String var1, String var2);

        void info(String var1, String var2);

        void error(String var1, String var2);

        void info(String var1, String var2, Object... var3);

        void debug(String var1, String var2, Object... var3);

        void error(String var1, String var2, Object... var3);

        void logException(String var1, Throwable var2);
    }
}
