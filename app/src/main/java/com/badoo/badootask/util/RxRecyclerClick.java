package com.badoo.badootask.util;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import rx.Observable;
import rx.Subscriber;
import rx.android.MainThreadSubscription;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class RxRecyclerClick {

    public static Observable<Integer> onItemClick(RecyclerView recyclerView) {
        return Observable.create(new AdapterViewItemClickOnSubscribe(recyclerView));
    }


    static class AdapterViewItemClickOnSubscribe implements Observable.OnSubscribe<Integer> {

        private final RecyclerView recyclerView;

        public AdapterViewItemClickOnSubscribe(RecyclerView view) {
            recyclerView = view;
        }

        @Override
        public void call(final Subscriber<? super Integer> subscriber) {
            Context context = recyclerView.getContext();
            RecyclerItemClickListener.OnItemClickListener clickListener = new RecyclerItemClickListener.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    if (!subscriber.isUnsubscribed()) {
                        subscriber.onNext(position);
                    }
                }
            };

            final RecyclerItemClickListener listener = new RecyclerItemClickListener(context, clickListener);
            recyclerView.addOnItemTouchListener(listener);

            subscriber.add(new MainThreadSubscription() {
                @Override protected void onUnsubscribe() {
                   recyclerView.removeOnItemTouchListener(listener);
                }
            });
        }
    }
}
