package com.badoo.badootask.util;

import com.google.gson.Gson;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class GsonHelper {
    public final static Gson GSON = new Gson();
}
