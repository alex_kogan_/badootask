package com.badoo.badootask;

import android.app.Application;
import android.support.annotation.NonNull;

import com.badoo.badootask.data.ProductDataStore;
import com.badoo.badootask.data.RateDataStore;
import com.badoo.badootask.domain.usecases.GetProductDetailsUseCase;
import com.badoo.badootask.domain.usecases.GetProductsUseCase;
import com.badoo.badootask.presentation.products.ProductsPresenter;
import com.badoo.badootask.presentation.transactions.TransactionsPresenter;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class TestApp extends Application {

    private ProductDataStore productDataStore;
    private RateDataStore ratesDataStore;

    public final static String MAIN_CURRENCY = "GBP";
    public final static String TX_JSON = "transactions1.json";
    public final static String RATE_JSON = "rates1.json";

    @Override
    public void onCreate() {
        super.onCreate();
        productDataStore = ProductDataStore.with(TestApp.this);
        ratesDataStore = RateDataStore.with(TestApp.this);
    }

    public ProductsPresenter createProductsPresenter() {
        GetProductsUseCase getProductDetailsUseCase = createGetProductsUseCase();
        return ProductsPresenter.newInstance(getProductDetailsUseCase);
    }

    public TransactionsPresenter createTransactionsPresenter() {
        GetProductDetailsUseCase getProductDetailsUseCase = createGetProductDetailsUseCase();
        return TransactionsPresenter.newInstance(getProductDetailsUseCase);
    }

    @NonNull
    private GetProductsUseCase createGetProductsUseCase() {
        return GetProductsUseCase.with(productDataStore);
    }

    @NonNull
    private GetProductDetailsUseCase createGetProductDetailsUseCase() {
        return GetProductDetailsUseCase.with(ratesDataStore, productDataStore);
    }
}
