package com.badoo.badootask.data;

import android.content.Context;

import com.badoo.badootask.TestApp;
import com.badoo.badootask.data.graph.Edge;
import com.badoo.badootask.data.graph.Graph;
import com.badoo.badootask.util.GsonHelper;
import com.badoo.badootask.util.JsonFileReader;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;


/**
 * Created by alexkogan on 21/05/2016.
 */
public class RateDataStore {

    private final Context context;
    private Graph<String> graph;
    private final Map<String, HashMap<String, Double>> mShortestPathMap = new HashMap<>();
    private boolean mJsonRead;

    public static RateDataStore with(Context context) {
        return new RateDataStore(context);
    }

    private RateDataStore(Context context) {
        this.context = context;
    }

    private void readJson() {
        if (mJsonRead) return;
        String json = JsonFileReader.readFile(context, TestApp.RATE_JSON);
        Rate[] rateArray = GsonHelper.GSON.fromJson(json, Rate[].class);
        List<Rate> rates = rateArray != null ? Arrays.asList(rateArray) : Collections.EMPTY_LIST;

        // initalizing the currency set
        HashSet<String> currencySet = new HashSet<>();
        for (Rate rate : rates) {
            currencySet.add(rate.getFrom());
            currencySet.add(rate.getTo());
        }

        // Adding first edges
        graph = new Graph<>(true);
        for (String currency : currencySet) {
            graph.add(currency, new ArrayList<Edge<String>>());
        }

        for (Rate rate : rates) {
            graph.addArc(rate.getFrom(), rate.getTo(), rate.getRate());
        }

        mJsonRead = true;
    }

    public BigDecimal calcConvertedAmount(BigDecimal amount, String to, String from) {
        if (to.equals(from))
            return amount;

        readJson();

        HashMap<String, Double> map;
        if ((map = mShortestPathMap.get(from)) == null) {
            map = Graph.dijkstraShortestPath(graph, from);
            mShortestPathMap.put(from, map);
        }

        Double rate = map.get(to);
        if (rate != null && rate != 0 && !rate.equals(Double.POSITIVE_INFINITY)) {
            return BigDecimal.valueOf(rate).multiply(amount);
        }
        return BigDecimal.ZERO;
    }
}
