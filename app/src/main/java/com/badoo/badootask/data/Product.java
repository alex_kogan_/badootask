package com.badoo.badootask.data;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class Product {

    public static final String ID_KEY = "id";

    public static Product with(String sku, int count) {
        Product product = new Product();
        product.sku = sku;
        product.transactionCount = count;
        return product;
    }

    private String sku;
    private int transactionCount;

    public String getSku() {
        return sku;
    }

    public int getTransactionCount() {
        return transactionCount;
    }
}
