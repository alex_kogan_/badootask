package com.badoo.badootask.data;

import java.math.BigDecimal;

/**
 * Created by alexkogan on 21/05/2016.
 */
class Rate {

    private String from;
    private String to;
    private BigDecimal rate;

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public BigDecimal getRate() {
        return rate;
    }
}
