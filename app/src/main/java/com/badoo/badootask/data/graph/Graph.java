package com.badoo.badootask.data.graph;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class Graph<V> {

    private final HashMap<V, ArrayList<Edge<V>>> adjacencyList;

    private final ArrayList<V> vertexList;

    private final boolean directed;

    public Graph(boolean isDirected) {
        directed = isDirected;
        adjacencyList = new HashMap<>();
        vertexList = new ArrayList<>();
    }

    public void add(V vertex, ArrayList<Edge<V>> connectedVertices) {
        // Add the new vertex to the adjacencyList with it's list of connected
        // nodes
        adjacencyList.put(vertex, connectedVertices);
        vertexList.add(vertex);
        // If this is an undirected graph, every edge needs to represented
        // twice, once in the added vertex's list and once in the list of each
        // of the vertex's connected to the added vertex

        for (Edge<V> vertexConnectedToAddedVertex : connectedVertices) {
            ArrayList<Edge<V>> correspondingConnectedList = adjacencyList
                    .get(vertexConnectedToAddedVertex.getVertex());
            // The added vertex's connections might not be represented in
            // the Graph yet, so we implicitly add them
            if (correspondingConnectedList == null) {
                adjacencyList.put(vertexConnectedToAddedVertex.getVertex(),
                        new ArrayList<Edge<V>>());
                vertexList.add(vertexConnectedToAddedVertex.getVertex());
                correspondingConnectedList = adjacencyList
                        .get(vertexConnectedToAddedVertex.getVertex());
            }

            if (!directed) {
                // The weight from one vertex back to another in an undirected
                // graph is equal
                Object weight = vertexConnectedToAddedVertex.getWeight();
                correspondingConnectedList.add(new Edge<>(vertex, weight));
            }
        }

    }

    public boolean addArc(V source, V end, Object weight) {
        if (!directed) {
            return false;
        }

        if (!adjacencyList.containsKey(source)) {
            ArrayList<Edge<V>> tempList = new ArrayList<>();
            tempList.add(new Edge<>(end, weight));
            add(source, tempList);
            return true;
        }

        if (!adjacencyList.containsKey(end)) {
            ArrayList<Edge<V>> tempList = new ArrayList<>();
            add(end, tempList);
        }


        adjacencyList.get(source).add(new Edge<>(end, weight));
        return true;
    }

    /**
     * This method returns a list of all adjacent vertices of the give vertex without weight
     *
     * @param vertex the source vertex
     * @return an array list containing the vertices
     */
    ArrayList<V> getAdjacentVertices(V vertex){
        ArrayList<V> returnList = new ArrayList<>();
        final ArrayList<Edge<V>> edges = adjacencyList.get(vertex);
        if (edges != null) {
            for (Edge<V> edge : edges) {
                returnList.add(edge.getVertex());
            }
        }
        return returnList;

    }

    double getDistanceBetween(V source, V end){
        for (Edge<V> edge : adjacencyList.get(source)) {
            if (edge.getVertex() == end){
                return ((BigDecimal)edge.getWeight()).doubleValue();
            }
        }
        return Double.POSITIVE_INFINITY;
    }

    ArrayList<V> getVertexList() {
        return vertexList;
    }

    public String toString() {
        String s = "";
        for (V vertex : vertexList) {
            s += vertex.toString();
            s += " : ";
            s += adjacencyList.get(vertex);
            s += "\n";
        }
        return s;
    }

    public static <V> HashMap<V, Double> dijkstraShortestPath(Graph<V> graph,
                                                              V source) {
        HashMap<V, Double> distances = new HashMap<>();
        ArrayList<V> queue = new ArrayList<>();
        ArrayList<V> visited = new ArrayList<>();
        queue.add(0, source);
        distances.put(source, 1.0);
        while (!queue.isEmpty()) {

            V currentVertex = queue.remove(queue.size() - 1);

            // to save time we initialize all the distances to infinity as we go
            if (distances.get(currentVertex) == null) {
                distances.put(currentVertex, Double.POSITIVE_INFINITY);
            }
            for (V adjacentVertex :  graph.getAdjacentVertices(currentVertex)) {
                if (distances.get(adjacentVertex) == null) {
                    distances.put(adjacentVertex, Double.POSITIVE_INFINITY);
                }

                // if the distance between the source and the adjacent vertex is
                // greater than the distance between the source and the current
                // vertex PLUS the weight between the current and adjacent
                // vertex, then we have found a shorter path than already
                // existed
                if (true) {

                    if (distances.get(adjacentVertex) > graph
                            .getDistanceBetween(currentVertex, adjacentVertex)
                            + distances.get(currentVertex)) {

                        distances.put(
                                adjacentVertex,
                                graph.getDistanceBetween(currentVertex, adjacentVertex)
                                        * distances.get(currentVertex));
                    }
                }

                if (!visited.contains(adjacentVertex)
                        && !queue.contains(adjacentVertex)) {
                    queue.add(0, adjacentVertex);
                }
            }
            visited.add(currentVertex);

        }

        // since the above statments only added the vertices as needed,
        // verticies that are completely unconnected to the source are not added
        // yet, so this adds them now
        for (V v : graph.getVertexList()) {
            if (!distances.containsKey(v)) {
                distances.put(v, Double.POSITIVE_INFINITY);
            }
        }

        return distances;
    }
}
