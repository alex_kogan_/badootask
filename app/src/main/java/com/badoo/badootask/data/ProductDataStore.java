package com.badoo.badootask.data;

import android.content.Context;

import com.badoo.badootask.TestApp;
import com.badoo.badootask.util.GsonHelper;
import com.badoo.badootask.util.JsonFileReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import rx.Observable;
import rx.Subscriber;


/**
 * Created by alexkogan on 21/05/2016.
 */
public class ProductDataStore {
    private final HashMap<String, List<Transaction>> mTransactionsMap = new HashMap<>();
    private final Context context;
    private boolean mJsonRead;

    public static ProductDataStore with(Context context) {
        return new ProductDataStore(context);
    }

    private ProductDataStore(Context context) {
        this.context = context;
    }

    private void readJson() {
        if (mJsonRead) return;
        String json = JsonFileReader.readFile(context, TestApp.TX_JSON);
        Transaction[] transactions = GsonHelper.GSON.fromJson(json, Transaction[].class);

        if (transactions != null) {
            for (Transaction transaction : transactions) {
                String sku = transaction.getSku();
                List<Transaction> array;
                if ((array = mTransactionsMap.get(sku)) == null)
                    mTransactionsMap.put(sku, array = new ArrayList<>());

                array.add(transaction);
            }
        }
        mJsonRead = true;
    }


    public Observable<List<Product>> getProducts() {
        return Observable.create(new Observable.OnSubscribe<List<Product>>() {
            @Override
            public void call(Subscriber<? super List<Product>> subscriber) {
                try {
                    readJson();
                    List<Product> products = new ArrayList<>();
                    for (String sku : mTransactionsMap.keySet()) {
                        int count = mTransactionsMap.get(sku).size();
                        products.add(Product.with(sku, count));
                    }
                    subscriber.onNext(products);
                    subscriber.onCompleted();
                } catch (Exception e) {
                    subscriber.onError(e);
                }
            }
        });
    }

    public Observable<List<Transaction>> getTransactions(String sku) {
        return Observable.just(mTransactionsMap.get(sku));
    }
}
