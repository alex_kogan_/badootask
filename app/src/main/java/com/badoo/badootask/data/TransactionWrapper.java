package com.badoo.badootask.data;

import java.math.BigDecimal;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class TransactionWrapper {
    private Transaction transaction;
    private BigDecimal convertedAmount;
    private String convertedCurrency;

    public BigDecimal getConvertedAmount() {
        return convertedAmount;
    }

    public String getConvertedCurrency() {
        return convertedCurrency;
    }

    public BigDecimal getOriginalAmount() {
        return transaction.getAmount();
    }

    public String getOriginalCurrency() {
        return transaction.getCurrency();
    }

    public static TransactionWrapper with(Transaction transaction, BigDecimal convertedAmount, String convertCurrency) {
        TransactionWrapper transactionWrapper = new TransactionWrapper();
        transactionWrapper.transaction = transaction;
        transactionWrapper.convertedAmount = convertedAmount;
        transactionWrapper.convertedCurrency = convertCurrency;
        return transactionWrapper;
    }
}
