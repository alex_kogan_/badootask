package com.badoo.badootask.data.graph;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class Edge<V> {

    private final V vertex;

    private final Object weight;

    public Edge(V vert, Object w) {
        vertex = vert;
        weight = w;
    }

    public V getVertex() {
        return vertex;
    }

    public Object getWeight() {
        return weight;
    }

    public String toString(){
        return "( "+ vertex + ", " + weight + " )";
    }

}
