package com.badoo.badootask.data;

import java.math.BigDecimal;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class Transaction {

    private BigDecimal amount;
    private String currency;
    private String sku;

    public BigDecimal getAmount() {
        return amount;
    }

    public String getCurrency() {
        return currency;
    }

    public String getSku() {
        return sku;
    }
}
