package com.badoo.badootask.domain.usecases;

import com.badoo.badootask.data.Product;
import com.badoo.badootask.data.ProductDataStore;

import java.util.List;

import rx.Observable;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class GetProductsUseCase extends UseCase<List<Product>> {

    private final ProductDataStore productDataStore;

    private GetProductsUseCase(ProductDataStore productDataStore) {
        this.productDataStore = productDataStore;
    }

    @Override
    protected Observable<List<Product>> buildUseCaseObservable() {
        return productDataStore.getProducts();
    }

    public static GetProductsUseCase with(ProductDataStore productDataStore) {
        return new GetProductsUseCase(productDataStore);
    }
}
