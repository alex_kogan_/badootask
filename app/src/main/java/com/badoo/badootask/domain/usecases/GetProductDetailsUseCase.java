package com.badoo.badootask.domain.usecases;

import com.badoo.badootask.data.ProductDataStore;
import com.badoo.badootask.data.RateDataStore;
import com.badoo.badootask.data.Transaction;
import com.badoo.badootask.data.TransactionWrapper;

import java.math.BigDecimal;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class GetProductDetailsUseCase extends UseCase<List<TransactionWrapper>> {

    private String productSku;
    private String convertCurrency;
    private final ProductDataStore productDataStore;
    private final RateDataStore rateDataStore;

    private GetProductDetailsUseCase(RateDataStore rateDataStore, ProductDataStore productDataStore) {
        this.rateDataStore = rateDataStore;
        this.productDataStore = productDataStore;
    }

    public static GetProductDetailsUseCase with(RateDataStore ratesDataStore, ProductDataStore productDataStore) {
        return new GetProductDetailsUseCase(ratesDataStore, productDataStore);
    }

    @Override
    protected Observable<List<TransactionWrapper>> buildUseCaseObservable() {
        return productDataStore.getTransactions(productSku)
                .flatMap(new Func1<List<Transaction>, Observable<Transaction>>() {
                    @Override
                    public Observable<Transaction> call(List<Transaction> transactions) {
                        return Observable.from(transactions);
                    }
                }).map(new Func1<Transaction, TransactionWrapper>() {
                    @Override
                    public TransactionWrapper call(Transaction transaction) {
                        BigDecimal convertedAmount = calcConvertedAmount(transaction);
                        return TransactionWrapper.with(transaction, convertedAmount, convertCurrency);
                    }
                }).toList();
    }

    private BigDecimal calcConvertedAmount(Transaction transaction) {
        String from = transaction.getCurrency();
        BigDecimal amount = transaction.getAmount();
        String to = convertCurrency;
        return rateDataStore.calcConvertedAmount(amount, to, from);
    }

    public GetProductDetailsUseCase setProductSku(String sku) {
        productSku = sku;
        return this;
    }

    public GetProductDetailsUseCase setConvertCurrency(String convertCurrency) {
        this.convertCurrency = convertCurrency;
        return this;
    }
}
