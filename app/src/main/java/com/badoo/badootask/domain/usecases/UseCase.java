package com.badoo.badootask.domain.usecases;


import com.badoo.badootask.util.BaseSubscriber;
import com.badoo.badootask.util.RxHelper;

import rx.Observable;
import rx.Subscription;

/**
 * Created by user on 8/20/15.
 */
public abstract class UseCase<T> {
    private Subscription mSubscription;

    public Observable<T> asObservable() {
        return UseCase.this.buildUseCaseObservable().compose(RxHelper.<T>applySchedulers());
    }


    @SuppressWarnings("unused")
    protected Subscription subscribeNetwork(Observable observable) {
        return observable.subscribe(new BaseSubscriber(UseCase.this.getClass().getSimpleName()));
    }

    protected abstract Observable<T> buildUseCaseObservable();


}
