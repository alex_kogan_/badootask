package com.badoo.badootask.presentation.products;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.badoo.badootask.presentation.Mvp;
import com.badoo.badootask.R;
import com.badoo.badootask.util.RxRecyclerClick;
import com.badoo.badootask.TestApp;
import com.badoo.badootask.presentation.viewholders.TwoTextViewHolder;
import com.badoo.badootask.data.Product;
import com.badoo.badootask.presentation.MainActivity;

import java.util.ArrayList;
import java.util.List;

import rx.Observable;
import rx.functions.Func1;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class ProductsFragment extends Mvp.View implements ProductsMvp.View {

    private ProductsAdapter adapter;
    private RecyclerView recyclerView;
    private TextView emptyView;

    @Override
    protected View onCreateView(LayoutInflater inflater, ViewGroup container) {
        final Context context = container.getContext();
        FrameLayout frameLayout = new FrameLayout(context);
        recyclerView = new RecyclerView(context);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        adapter = new ProductsAdapter();
        recyclerView.setAdapter(adapter);
        frameLayout.addView(recyclerView);


        emptyView = new TextView(context);
        emptyView.setText(R.string.EmptyViewText);
        emptyView.setVisibility(View.GONE);
        emptyView.setTextSize(16);
        emptyView.setTextColor(Color.GRAY);
        emptyView.setGravity(Gravity.CENTER);
        FrameLayout.LayoutParams params = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
        frameLayout.addView(emptyView, params);
        return frameLayout;
    }

    @Override
    protected Mvp.Presenter createPresenter() {
        return ((TestApp)getActivity().getApplication()).createProductsPresenter();
    }

    @Override
    public void showProducts(List<Product> products) {
        if (products.isEmpty()) {
            emptyView.setVisibility(View.VISIBLE);
        } else {
            adapter.setProducts(products);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public Observable<Product> createProductSelectObservable() {
        return RxRecyclerClick.onItemClick(recyclerView)
                .map(new Func1<Integer, Product>() {
                    @Override
                    public Product call(Integer integer) {
                        return adapter.getProduct(integer);
                    }
                });
    }

    @Override
    public void showProductDetails(Bundle bundle) {
        ((MainActivity)getActivity()).showProductDetails(bundle);
    }

    class ProductsAdapter extends RecyclerView.Adapter<TwoTextViewHolder> {

        private final List<Product> mProducts = new ArrayList<>();

        public void setProducts(List<Product> products) {
            mProducts.clear();
            mProducts.addAll(products);
        }

        @Override
        public TwoTextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new TwoTextViewHolder(LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.list_row_two_text, parent, false));
        }

        @Override
        public void onBindViewHolder(TwoTextViewHolder holder, int position) {
            Context context = holder.itemView.getContext();
            Product product = mProducts.get(position);
            holder.text1.setText(product.getSku());
            holder.text2.setText(context.getString(R.string.TransactionRowContent, product.getTransactionCount()));
        }

        @Override
        public int getItemCount() {
            return mProducts.size();
        }

        public Product getProduct(Integer integer) {
            return mProducts.get(integer);
        }
    }


}
