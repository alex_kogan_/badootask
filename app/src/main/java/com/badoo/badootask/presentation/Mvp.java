package com.badoo.badootask.presentation;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.badoo.badootask.util.BaseSubscriber;
import com.badoo.badootask.util.Logger;

import rx.Observable;
import rx.Subscription;
import rx.internal.util.SubscriptionList;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class Mvp {

    public interface Model {

    }

    static public abstract class View extends Fragment {
        private SubscriptionList subscriptionList;
        private Presenter mPresenter;

        @Nullable
        @Override
        final public android.view.View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            subscriptionList = new SubscriptionList();
            return onCreateView(inflater, container);
        }

        @Override
        public void onViewCreated(android.view.View view, Bundle savedInstanceState) {
            super.onViewCreated(view, savedInstanceState);
            if (getPresenter() != null)
                //noinspection unchecked
                getPresenter().create(this);
        }

        @Override
        public void onStart() {
            super.onStart();
            if (getPresenter() != null)
                getPresenter().start();
        }

        @Override
        public void onResume() {
            super.onResume();
            if (getPresenter() != null)
                getPresenter().resume();
        }

        @Override
        public void onPause() {
            super.onPause();
            if (getPresenter() != null)
                getPresenter().pause();
        }

        @Override
        public void onStop() {
            super.onStop();
            if (getPresenter() != null)
                getPresenter().stop();
        }

        @Override
        public void onDestroyView() {
            super.onDestroyView();
            if (subscriptionList != null && !subscriptionList.isUnsubscribed())
                subscriptionList.unsubscribe();

            if (getPresenter() != null)
                getPresenter().destroy();
        }

        private Presenter getPresenter() {
            if (mPresenter == null)
                mPresenter = createPresenter();
            return mPresenter;
        }

        protected abstract Presenter createPresenter();

        protected android.view.View onCreateView(LayoutInflater inflater, ViewGroup container) {
            return null;
        }

    }

    static public abstract class Presenter {
        private final SubscriptionList mSubscriptionList = new SubscriptionList();
        View mView;

        protected void onCreate(View view) {

        }

        void onStart() {

        }

        void onResume() {

        }

        void onPause() {

        }

        void onStop() {

        }

        void onDestroy() {

        }

        public final void create(View view) {
            Logger.debug(super.getClass().getSimpleName(), "create");
            onCreate(mView = view);
        }

        public final void start() {
            Logger.debug(super.getClass().getSimpleName(), "start");
            onStart();
        }

        public final void resume() {
            Logger.debug(super.getClass().getSimpleName(), "resume");
            onResume();
        }

        public final void pause() {
            Logger.debug(super.getClass().getSimpleName(), "pause");
            onPause();
        }

        public final void stop() {
            Logger.debug(super.getClass().getSimpleName(), "stop");
            onStop();
        }

        public final void destroy() {
            Logger.debug(super.getClass().getSimpleName(), "destroy");
            onDestroy();
            mView = null;
        }

        void addSubscription(Subscription subscription) {
            if (mSubscriptionList == null || mSubscriptionList.isUnsubscribed())
                throw new RuntimeException("Make sure to call super.initObservables() !!!");
            mSubscriptionList.add(subscription);
        }

        protected final Subscription subscribe(Observable observable) {
            final Subscription subscribe = observable.subscribe(new BaseSubscriber(this.getClass().getSimpleName()));
            addSubscription(subscribe);
            return subscribe;
        }
    }
}
