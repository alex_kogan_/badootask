package com.badoo.badootask.presentation.products;

import android.os.Bundle;

import com.badoo.badootask.data.Product;
import com.badoo.badootask.presentation.Mvp;

import java.util.List;

import rx.Observable;

/**
 * Created by alexkogan on 21/05/2016.
 */
public interface ProductsMvp {

    interface Model extends Mvp.Model {
    }

    interface View  {
        void showProducts(List<Product> products);

        Observable<Product> createProductSelectObservable();

        void showProductDetails(Bundle bundle);
    }

    interface Presenter {

    }
}
