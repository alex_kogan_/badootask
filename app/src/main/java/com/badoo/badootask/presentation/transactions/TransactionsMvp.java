package com.badoo.badootask.presentation.transactions;

import android.os.Bundle;

import com.badoo.badootask.data.TransactionWrapper;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by alexkogan on 21/05/2016.
 */
public interface TransactionsMvp {

    interface Model {
    }

    interface View {
        Bundle getArguments();

        void showTransactions(BigDecimal total, List<TransactionWrapper> transactions);
    }

    interface Presenter {
    }
}
