package com.badoo.badootask.presentation.transactions;

import android.os.Bundle;

import com.badoo.badootask.TestApp;
import com.badoo.badootask.data.Product;
import com.badoo.badootask.data.TransactionWrapper;
import com.badoo.badootask.domain.usecases.GetProductDetailsUseCase;
import com.badoo.badootask.presentation.Mvp;

import java.math.BigDecimal;
import java.util.List;

import rx.functions.Action1;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class TransactionsPresenter extends Mvp.Presenter implements TransactionsMvp.Presenter {

    private final GetProductDetailsUseCase getProductDetailsUseCase;

    public static TransactionsPresenter newInstance(GetProductDetailsUseCase useCase) {
        return new TransactionsPresenter(useCase);
    }

    private TransactionsPresenter(GetProductDetailsUseCase useCase) {
        getProductDetailsUseCase = useCase;
    }

    @Override
    protected void onCreate(Mvp.View v) {
        final TransactionsMvp.View view = (TransactionsMvp.View) v;
        Bundle bundle = view.getArguments();
        String id = bundle.getString(Product.ID_KEY);

        subscribe(getProductDetailsUseCase.setProductSku(id)
                .setConvertCurrency(TestApp.MAIN_CURRENCY).asObservable()
                .doOnNext(new Action1<List<TransactionWrapper>>() {
                    @Override
                    public void call(List<TransactionWrapper> transactions) {
                        view.showTransactions(calcTotal(transactions), transactions);
                    }
                }).doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));
    }

    private BigDecimal calcTotal(List<TransactionWrapper> transactions) {
        BigDecimal value = BigDecimal.ZERO;
        for (TransactionWrapper transaction : transactions) {
            value = value.add(transaction.getConvertedAmount());
        }
        return value;
    }
}
