package com.badoo.badootask.presentation.transactions;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.badoo.badootask.presentation.Mvp;
import com.badoo.badootask.R;
import com.badoo.badootask.TestApp;
import com.badoo.badootask.data.TransactionWrapper;
import com.badoo.badootask.presentation.viewholders.TwoTextViewHolder;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Currency;
import java.util.List;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class TransactionsFragment extends Mvp.View implements TransactionsMvp.View {

    private final static DecimalFormat DECIMAL_FORMAT = new DecimalFormat("#,###,###.##");

    private DetailsAdapter adapter;

    @Override
    protected View onCreateView(LayoutInflater inflater, ViewGroup container) {
        final Context context = container.getContext();
        RecyclerView recyclerView = new RecyclerView(context);
        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setBackgroundColor(Color.WHITE);
        adapter = new DetailsAdapter();
        recyclerView.setAdapter(adapter);
        return recyclerView;
    }

    @Override
    protected Mvp.Presenter createPresenter() {
        return ((TestApp)getActivity().getApplication()).createTransactionsPresenter();
    }

    @Override
    public void showTransactions(BigDecimal total, List<TransactionWrapper> transactions) {
        adapter.setTotalAmount(total);
        adapter.setTransactions(transactions);
        adapter.notifyDataSetChanged();
    }



    class DetailsAdapter extends RecyclerView.Adapter<TwoTextViewHolder> {
        private BigDecimal mTotalAmount = BigDecimal.ZERO;
        private final List<TransactionWrapper> mTransactions = new ArrayList<>();

        @Override
        public TwoTextViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            LayoutInflater inflater = LayoutInflater.from(parent.getContext());
            int layoutId = (viewType == 0) ? R.layout.list_row_header : R.layout.list_row_two_text;
            return new TwoTextViewHolder(inflater.inflate(layoutId, parent, false));
        }

        @SuppressLint("SetTextI18n")
        @Override
        public void onBindViewHolder(TwoTextViewHolder holder, int position) {
            Context context = holder.itemView.getContext();
            if (position == 0) {
                final String convertedCurrency = Currency.getInstance(TestApp.MAIN_CURRENCY).getSymbol();
                holder.text1.setText(context.getString(R.string.TransactionTotalTitle, convertedCurrency + DECIMAL_FORMAT.format(mTotalAmount)));
            } else {
                int index = position - 1;
                TransactionWrapper transactionWrapper = mTransactions.get(index);
                final String originalCurrency = Currency.getInstance(transactionWrapper.getOriginalCurrency().toUpperCase()).getSymbol();
                holder.text1.setText(originalCurrency + DECIMAL_FORMAT.format(transactionWrapper.getOriginalAmount()));

                final String convertedCurrency = Currency.getInstance(transactionWrapper.getConvertedCurrency().toUpperCase()).getSymbol();
                holder.text2.setText(convertedCurrency + DECIMAL_FORMAT.format(transactionWrapper.getConvertedAmount()));
            }
        }

        @Override
        public int getItemCount() {
            return mTransactions.size() + 1;
        }

        @Override
        public int getItemViewType(int position) {
            return position == 0 ? 0 : 1;
        }

        public void setTotalAmount(BigDecimal totalAmount) {
            mTotalAmount = totalAmount;
        }

        public void setTransactions(List<TransactionWrapper> transactions) {
            mTransactions.clear();
            mTransactions.addAll(transactions);
        }
    }

}
