package com.badoo.badootask.presentation.products;

import android.os.Bundle;

import com.badoo.badootask.data.Product;
import com.badoo.badootask.domain.usecases.GetProductsUseCase;
import com.badoo.badootask.presentation.Mvp;

import java.util.List;

import rx.functions.Action1;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class ProductsPresenter extends Mvp.Presenter implements ProductsMvp.Presenter {

    private static final String TAG = ProductsPresenter.class.getSimpleName();
    private final GetProductsUseCase getProductsUseCase;
    private ProductsMvp.View view;

    public static ProductsPresenter newInstance(GetProductsUseCase useCase) {
        return new ProductsPresenter(useCase);
    }

    private ProductsPresenter(GetProductsUseCase useCase) {
        getProductsUseCase = useCase;
    }

    @Override
    protected void onCreate(Mvp.View v) {
        view = (ProductsMvp.View) v;
        subscribe(getProductsUseCase.asObservable()
                .doOnNext(new Action1<List<Product>>() {
                    @Override
                    public void call(List<Product> products) {
                        view.showProducts(products);
                    }
                }).doOnError(new Action1<Throwable>() {
                    @Override
                    public void call(Throwable throwable) {

                    }
                }));

        subscribe(view.createProductSelectObservable().doOnNext(new Action1<Product>() {
            @Override
            public void call(Product product) {
                Bundle bundle = new Bundle();
                final String sku = product.getSku();
                bundle.putString(Product.ID_KEY, sku);
                view.showProductDetails(bundle);
            }
        }));
    }
}
