package com.badoo.badootask.presentation;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.badoo.badootask.presentation.products.ProductsFragment;
import com.badoo.badootask.R;
import com.badoo.badootask.presentation.transactions.TransactionsFragment;
import com.badoo.badootask.data.Product;

public class MainActivity extends AppCompatActivity {

    private String toolbarTitle;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        ToolbarState.PRODUCTS.setToolbar(MainActivity.this);

        Fragment fragment = createFragment(ProductsFragment.class, Bundle.EMPTY);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container, fragment);
        ft.commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
                onBackPressed();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private Fragment createFragment(Class clz, Bundle arguments) {
        return Fragment.instantiate(MainActivity.this, clz.getName(), arguments);
    }


    public void showProductDetails(Bundle bundle) {
        String sku = bundle.getString(Product.ID_KEY);
        toolbarTitle = getString(R.string.TransactionFragmentTitle, sku);

        ToolbarState.TRANSACTIONS.setToolbar(MainActivity.this);

        Fragment fragment = createFragment(TransactionsFragment.class, bundle);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        ft.add(R.id.container, fragment);
        ft.addToBackStack(null);
        ft.commit();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0) {
            ToolbarState.PRODUCTS.setToolbar(MainActivity.this);
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    private String getToolbarTitle() {
        return toolbarTitle;
    }

    enum ToolbarState {
        PRODUCTS() {
            @Override
            void setToolbar(MainActivity activity) {
                activity.toolbarTitle = activity.getString(R.string.ProductsTitle);
                activity.toolbar.setNavigationIcon(R.drawable.home);
                //noinspection ConstantConditions
                activity.getSupportActionBar().setTitle(activity.getToolbarTitle());
                activity.getSupportActionBar().setHomeButtonEnabled(false);
            }
        },
        TRANSACTIONS() {
            @Override
            void setToolbar(MainActivity activity) {
                activity.toolbar.setNavigationIcon(R.drawable.arrow_left);
                //noinspection ConstantConditions
                activity.getSupportActionBar().setTitle(activity.getToolbarTitle());
                activity.getSupportActionBar().setHomeButtonEnabled(true);
            }
        };

        void setToolbar(MainActivity activity) {

        }
    }
}
