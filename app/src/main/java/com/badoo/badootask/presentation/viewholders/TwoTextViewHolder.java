package com.badoo.badootask.presentation.viewholders;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;

import com.badoo.badootask.R;

/**
 * Created by alexkogan on 21/05/2016.
 */
public class TwoTextViewHolder extends RecyclerView.ViewHolder {

    public final TextView text1;
    public final TextView text2;

    public TwoTextViewHolder(View itemView) {
        super(itemView);

        text1 = (TextView) itemView.findViewById(R.id.text1);
        text2 = (TextView) itemView.findViewById(R.id.text2);
    }
}
